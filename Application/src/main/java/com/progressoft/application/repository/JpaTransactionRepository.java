package com.progressoft.application.repository;

import com.progressoft.application.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaTransactionRepository extends JpaRepository<TransactionEntity, Long>
        , JpaSpecificationExecutor<TransactionEntity> {

    List<TransactionEntity> findAllByCustomerId(String customerId);

    List<TransactionEntity> findAllByAccountNumberAndCustomerId(long accountNumber, String customerId);

}
