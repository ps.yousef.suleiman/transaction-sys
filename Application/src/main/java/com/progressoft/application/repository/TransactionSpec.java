package com.progressoft.application.repository;

import com.progressoft.application.entity.TransactionEntity;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.LessThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;

@And({
        @Spec(params = "customerId", path = "customerId", spec = Equal.class),
        @Spec(params = "accountNumber", path = "accountNumber", spec = Equal.class),
        @Spec(params = "from", path = "amount", spec = GreaterThanOrEqual.class),
        @Spec(params = "to", path = "amount", spec = LessThanOrEqual.class)

})
public interface TransactionSpec extends Specification<TransactionEntity> {
}
