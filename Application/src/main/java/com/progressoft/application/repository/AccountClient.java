package com.progressoft.application.repository;

import com.progressoft.model.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "accounts", url = "${account.server.url}")
public interface AccountClient {

    @GetMapping("/{customerId}/{accountNumber}")
    Account getAccountByCustomerIdAndAccountNumber(@PathVariable String customerId, @PathVariable long accountNumber);
}
