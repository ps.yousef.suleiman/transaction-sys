package com.progressoft.application.mapper;

import com.progressoft.application.entity.TransactionEntity;
import com.progressoft.application.resources.TransactionRequest;
import com.progressoft.application.resources.TransactionResponse;
import com.progressoft.model.Transaction;
import com.progressoft.model.TransactionType;
import org.springframework.stereotype.Service;

@Service
public class TransactionMapper {
    public TransactionEntity toTransactionEntity(Transaction transaction) {
        return TransactionEntity.builder()
                .id(transaction.getId())
                .accountNumber(transaction.getAccountNumber())
                .transactionType(transaction.getTransactionType())
                .transactionTime(transaction.getTransactionTime())
                .amount(transaction.getAmount())
                .customerId(transaction.getCustomerId())
                .build();
    }

    public Transaction toTransaction(TransactionEntity transactionEntity) {
        return Transaction.builder()
                .id(transactionEntity.getId())
                .accountNumber(transactionEntity.getAccountNumber())
                .transactionType(transactionEntity.getTransactionType())
                .amount(transactionEntity.getAmount())
                .customerId(transactionEntity.getCustomerId())
                .transactionTime(transactionEntity.getTransactionTime())
                .build();
    }

    public TransactionResponse toResponseTransaction(Transaction transaction) {
        return TransactionResponse.builder()
                .customerId(transaction.getCustomerId())
                .amount(transaction.getAmount())
                .accountNumber(transaction.getAccountNumber())
                .transactionTime(transaction.getTransactionTime())
                .transactionType(transaction.getTransactionType())
                .build();
    }

    public Transaction toTransaction(TransactionRequest transactionRequest) {
        return Transaction.builder()
                .transactionType(TransactionType.valueOf(transactionRequest.transactionType().toUpperCase()))
                .accountNumber(transactionRequest.accountNumber())
                .amount(transactionRequest.amount())
                .customerId(transactionRequest.customerId())
                .build();
    }

    public TransactionResponse toResponseTransaction(TransactionEntity transactionEntity) {
        return TransactionResponse.builder()
                .customerId(transactionEntity.getCustomerId())
                .amount(transactionEntity.getAmount())
                .accountNumber(transactionEntity.getAccountNumber())
                .transactionTime(transactionEntity.getTransactionTime())
                .transactionType(transactionEntity.getTransactionType())
                .build();
    }
}
