package com.progressoft.application.controller;

import com.progressoft.application.mapper.TransactionMapper;
import com.progressoft.application.repository.JpaTransactionRepository;
import com.progressoft.application.repository.TransactionSpec;
import com.progressoft.application.resources.TransactionRequest;
import com.progressoft.application.resources.TransactionResponse;
import com.progressoft.model.Transaction;
import com.progressoft.repository.TransactionRepository;
import com.progressoft.usecases.CreateTransaction;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/transaction")
public class TransactionController {
    private final TransactionRepository transactionRepository;
    private final TransactionMapper mapper;
    private final JpaTransactionRepository jpaTransactionRepository;
    private final CreateTransaction createTransaction;

    @GetMapping
    public Page<TransactionResponse> getAllTransaction(Pageable pageable, TransactionSpec spec) {
        return jpaTransactionRepository
                .findAll(spec, pageable).map(mapper::toResponseTransaction);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public void insert(@RequestBody TransactionRequest transactionRequest) {
        Transaction transaction = mapper.toTransaction(transactionRequest);
        createTransaction.execute(transaction);
        log.info("Transaction Added {}", transactionRequest.customerId());
    }

    @GetMapping("{customerId}/{accountNumber}")
    public List<TransactionResponse> getTransactionByCustomerIdAndAccountNumber(@PathVariable String accountNumber, @PathVariable String customerId) {
        return transactionRepository
                .findAllByAccountNumberAndCustomerId(Long.valueOf(accountNumber), customerId)
                .stream()
                .map(mapper::toResponseTransaction)
                .collect(Collectors.toList());
    }

    @GetMapping("{customerId}")
    public List<TransactionResponse> getTransactionByCustomerId(@PathVariable String customerId) {
        return transactionRepository
                .findAllByCustomerId(customerId)
                .stream()
                .map(mapper::toResponseTransaction)
                .collect(Collectors.toList());
    }
}
