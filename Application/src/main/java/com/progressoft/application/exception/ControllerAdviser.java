package com.progressoft.application.exception;

import com.progressoft.exception.Violation;
import com.progressoft.exception.ViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Set;

@ControllerAdvice
public class ControllerAdviser extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ViolationException.class})
    public ResponseEntity<Set<Violation>> handleViolationException(ViolationException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getViolations(), HttpStatus.BAD_REQUEST);
    }

}
