package com.progressoft.application.controller;

import com.progressoft.application.repository.AccountClient;
import com.progressoft.model.Account;
import com.progressoft.model.Transaction;
import com.progressoft.model.TransactionType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountProviderTest {

    @Mock
    private AccountClient accountClient;
    @InjectMocks
    private AccountProviderImp accountProvider;

    @Test
    public void whenGetAccount() {
        when(accountClient.getAccountByCustomerIdAndAccountNumber(anyString(), anyLong())).thenReturn(Account.
                builder()
                .accountNumber(123456798123L)
                .creationDate(String.valueOf(LocalDateTime.now()))
                .availableBalance(BigDecimal.TEN)
                .status("Active")
                .build());

        Transaction transaction = new Transaction(123456798123L, "", 123456798123L,
                TransactionType.CREDIT, BigDecimal.TEN, LocalDateTime.now());

        Account account = accountProvider.getAccount(transaction.getCustomerId(), transaction.getAccountNumber());
        Assertions.assertThat(account).isNotNull();
        Assertions.assertThat(account.getAccountNumber()).isEqualTo(123456798123L);
    }

}