package com.progressoft.application.controller;

import com.progressoft.application.entity.TransactionEntity;
import com.progressoft.application.mapper.TransactionMapper;
import com.progressoft.application.repository.JpaTransactionRepository;
import com.progressoft.model.Transaction;
import com.progressoft.model.TransactionType;
import com.progressoft.repository.TransactionRepository;
import com.progressoft.usecases.CreateTransaction;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = TransactionController.class)
@ExtendWith(SpringExtension.class)
class TransactionControllerTest {

    @MockBean
    TransactionRepository transactionRepository;

    @MockBean
    JpaTransactionRepository jpaTransactionRepository;

    @MockBean
    TransactionMapper transactionMapper;

    @MockBean
    CreateTransaction createTransaction;

    @Autowired
    MockMvc mockMvc;

    @Test
    @Disabled
    void whenGetAllTransaction_thenExpectedResultReturned() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        TransactionEntity transactionEntity = TransactionEntity.builder()
                .transactionType(TransactionType.CREDIT)
                .amount(BigDecimal.TEN)
                .accountNumber(11111L)
                .transactionTime(now)
                .customerId("KHALED")
                .id(2L)
                .build();
        when(transactionMapper.toTransactionEntity(any(Transaction.class)))
                .thenReturn(TransactionEntity.builder()
                        .transactionType(TransactionType.CREDIT)
                        .amount(BigDecimal.TEN)
                        .accountNumber(11111L)
                        .transactionTime(now)
                        .customerId("KHALED")
                        .build());
        when(jpaTransactionRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(List.of(transactionEntity)));

        String json = "{\"content\":[null],\"pageable\":\"INSTANCE\",\"totalPages\":1,\"totalElements\":1,\"last\":true,\"size\":1,\"number\":0,\"sort\":{\"empty\":true,\"sorted\":false,\"unsorted\":true},\"numberOfElements\":1,\"first\":true,\"empty\":false}";
        mockMvc.perform(get("/api/v1/transaction")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    Assertions.assertThat(result.getResponse().getContentAsString()).isEqualTo(json);
                })
                .andExpect(status().isOk());

        Pageable pageable = Pageable.ofSize(20);

        verify(jpaTransactionRepository).findAll(pageable);
    }

    @Test
    void givenValidTransaction_whenInsert_thenExpectedResultIsReturned() throws Exception {
        String json = "{\n" +
                "    \"accountNumber\" : \"2700647964782413\",\n" +
                "    \"customerId\" : \"TAYSEER\" ,\n" +
                "    \"amount\" : \"100\" ,\n" +
                "    \"transactionType\" : \"Debit\"\n" +
                "}";

        mockMvc.perform(post("/api/v1/transaction")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void whenGetTransactionsByCustomerId_thenExpectedResultReturned() throws Exception {
        Transaction transactionEntity = Transaction.builder()
                .transactionType(TransactionType.CREDIT)
                .amount(BigDecimal.TEN)
                .accountNumber(11111L)
                .transactionTime(LocalDateTime.now())
                .customerId("TAYSEER")
                .id(2L)
                .build();
        when(transactionRepository.findAllByCustomerId("TAYSEER")).thenReturn(List.of(transactionEntity));

        mockMvc.perform(get("/api/v1/transaction/TAYSEER")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}