package com.progressoft.validator;

import com.progressoft.exception.Violation;
import com.progressoft.model.Account;
import com.progressoft.model.Transaction;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@AllArgsConstructor
public class CreateTransactionValidator {

    private final AccountProvider accountProvider;

    public Set<Violation> validate(Transaction transaction) {
        Set<Violation> violations = new HashSet<>();

        if (Objects.isNull(transaction)) {
            violations.add(new Violation("Transaction", "Null Transaction"));
            return violations;
        }

        if (Objects.isNull(transaction.getAccountNumber())) {
            violations.add(new Violation("accountNumber", "Account Number is null"));
        }

        if (Objects.isNull(transaction.getCustomerId())) {
            violations.add(new Violation("customerId", "Customer Id is null"));
        }

        if (Objects.isNull(transaction.getAmount()) || transaction.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            violations.add(new Violation("amount", "Invalid Amount " + transaction.getAmount()));
        }

        try {
            Account account = accountProvider.getAccount(transaction.getCustomerId(), transaction.getAccountNumber());

            if (account.getAvailableBalance().compareTo(transaction.getAmount()) < 0) {
                violations.add(new Violation("Balance", "Not Enough Balance"));
            }

            if (account.getStatus().equals("Inactive")) {
                violations.add(new Violation("Account state", "Account is Inactive"));
            }

        } catch (RuntimeException ex) {
            violations.add(new Violation("accountNumber", "Account Not Found"));
        }
        return violations;
    }
}
