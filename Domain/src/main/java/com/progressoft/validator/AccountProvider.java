package com.progressoft.validator;

import com.progressoft.model.Account;

public interface AccountProvider {
    Account getAccount(String customerId, long accountNumber);
}
