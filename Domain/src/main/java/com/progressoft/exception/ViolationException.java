package com.progressoft.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
public class ViolationException extends RuntimeException {

    private final Set<Violation> violations;

    public ViolationException(Set<Violation> violations) {
        super();
        this.violations = violations;
    }
}
