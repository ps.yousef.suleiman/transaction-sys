package com.progressoft.validator;

import com.progressoft.exception.AccountNotFoundException;
import com.progressoft.exception.InvalidTransactionException;
import com.progressoft.exception.Violation;
import com.progressoft.model.Account;
import com.progressoft.model.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CreateTransactionValidatorTest {

    @Mock
    AccountProvider accountProvider;

    @InjectMocks
    CreateTransactionValidator createTransactionValidator;

    @Test
    public void givenInvalidTransaction_whenValidTransaction_thenThrowsReturned() {
        when(accountProvider.getAccount(anyString(), anyLong())).thenReturn(new Account());
        List<Violation> violations = createTransactionValidator.validate(null);
        Assertions.assertEquals(violations.get(0).getException().getClass(), InvalidTransactionException.class);

        Transaction transaction = new Transaction();
        violations = createTransactionValidator.validate(transaction);
        Assertions.assertEquals(violations.get(0).getException().getClass(), AccountNotFoundException.class);

        transaction = Transaction.builder().customerId("12334").build();
        violations = createTransactionValidator.validate(transaction);
        Assertions.assertEquals(violations.get(0).getException().getClass(), AccountNotFoundException.class);
        Assertions.assertEquals(violations.get(0).getException().getMessage(), "Account Number is null");

        transaction = Transaction.builder().customerId("12334").accountNumber(1242414L).build();
        violations = createTransactionValidator.validate(transaction);
        Assertions.assertEquals(violations.get(0).getException().getClass(), InvalidTransactionException.class);
        Assertions.assertEquals(violations.get(0).getException().getMessage(), "Invalid Amount null");
    }
}